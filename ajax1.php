<?php
// 郵便番号CSVファイル読込、配列データで返す
// (KEY:郵便番号 => array(住所1, 住所2, 住所3) )
function getZipCsv($filename) {
    // ZIPデータ配列
    $arrZip = array();
    // ファイルを開く
    $fp = fopen($filename, 'r');
    // whileで行末までループ処理
    while (!feof($fp)) {
        // fgetsでファイルを読み込み、変数に格納
        $txt = fgets($fp);
        if ($txt == "") continue;
        // SJISからUTF8への文字コード変換
        $txt = mb_convert_encoding($txt, 'UTF-8', 'sjis-win');
        // ["]を省く
        $txt = str_replace('"', '', $txt);
        // [,]で配列に分解
        $arr = explode(',', $txt);
        // 3カラム目をキーにして住所1, 住所2, 住所3を配列で退避
        $arrZip[$arr[2]] = array($arr[6], $arr[7], $arr[8]);
    }
    // fcloseでファイルを閉じる
    fclose($fp); 
    // 郵便番号配列データを返す
    return $arrZip;
}
 
// 郵便番号の取得
$zip = "";
if (isset($_POST["zip"]) == true && $_POST["zip"] != "") {
    $zip = $_POST["zip"];
}
 
// 全国の郵便番号CSVファイルを変数に格納
$arrZipData = getZipCsv('./KEN_ALL.CSV');
 
$arrRet = array( "", "", "");
if ($zip != "" && array_key_exists($zip, $arrZipData)) {
    // 郵便番号のキーが存在
    $arrRet = $arrZipData[$zip];
}
 
// 結果を返す
echo(json_encode($arrRet));
?>