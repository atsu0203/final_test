<?php
session_start();
session_regenerate_id(true);
require_once '../common/function.php';// for user-defined function

if(!empty($_POST['id'])){
    $_SESSION['id'] = $_POST['id'];
}
$db = dbConnect();
$sql = "SELECT * FROM products WHERE id = {$_SESSION['id']}";
//var_dump($sql);
$stmt = $db->prepare($sql);
$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM reviews WHERE product_id = {$_SESSION['id']} ORDER BY id DESC";
// var_dump($sql);
$stmt = $db->prepare($sql);
$stmt->execute();
$reviews = $stmt->fetchAll(PDO::FETCH_ASSOC);

$sql = "SELECT COUNT(product_id) FROM favorites WHERE product_id = {$_SESSION['id']}";
// var_dump($sql);
$stmt = $db->prepare($sql);
$stmt->execute();
$favorite = $stmt->fetch(PDO::FETCH_ASSOC);

if(isset($_SESSION['user_id'])){
$sql = "SELECT * FROM favorites WHERE product_id = {$_SESSION['id']} AND user_id = {$_SESSION['user_id']}";
// var_dump($sql);
$stmt = $db->prepare($sql);
$stmt->execute();
$favorited = $stmt->fetch(PDO::FETCH_ASSOC);
}



if (isset($_POST['delete'])) {
    try {
        $db = dbConnect();
        $db->beginTransaction();
        $sql = "DELETE FROM reviews WHERE id = {$_POST['pro_id']} LIMIT 1";
        //var_dump($sql);
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $db->commit();
    } catch (PDOException $e) {
        $db->rollBack();
        echo "接続失敗:" .$e->getMessage(). "\n";
    } finally {
        $db = null;
        header('Location: product_detail.php');
    }
}

if (isset($_POST['favorite'])) {
    try {
        if(empty($favorited)){
        $db = dbConnect();
        $db->beginTransaction();
        $user_id = intval($_SESSION['user_id']);
        $product_id = intval($_SESSION['id']);
        $sql = "INSERT INTO favorites(product_id, user_id)VALUES(:product_id, :user_id)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':product_id', $product_id, PDO::PARAM_STR);
        $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $stmt->execute();
        $db->commit();
        }
    } catch (PDOException $e) {
        $db->rollBack();
        echo "接続失敗:" .$e->getMessage(). "\n";
    } finally {
        $db = null;
        header('Location: product_detail.php');
    }
}


if(isset($_POST['favorite_delete'])){
    try {
        $db = dbConnect();
        $db->beginTransaction();
        $sql = "DELETE FROM favorites WHERE product_id = {$_SESSION['id']} AND user_id = {$_SESSION['user_id']} LIMIT 1";
        // var_dump($sql);
        $stmt = $db->prepare($sql);
        $stmt->execute(); 
        $db->commit();
    } catch (PDOException $e) {
        $db->rollBack();
        echo "接続失敗:" .$e->getMessage(). "\n";
    } finally {
        $db = null;
        header('Location: product_detail.php');
    } 
 }


// カートに追加機能
if (isset($_POST['add_cart'])) {
    // カートが空の場合の処理
    if (empty($_SESSION['cart'])) {
        $_SESSION['cart'] = array();
        // 商品IDと数量をセッションに代入
        $ary = array('id' => $_POST['pro_id'], 'amount' => $_POST['amount']);
        $_SESSION['cart'][] = $ary;
        header('Location: product_list.php');
    } else {
        // カートに同じ商品が入っているか確認
        foreach ($_SESSION['cart'] as $key => $val) {
            if ($val['id'] == $_POST['pro_id']) {
                $_SESSION['cart'][$key]['amount'] += $_POST['amount'];
                header('Location: product_list.php');
                exit();
            }
        }
        $ary = array('id' => $_POST['pro_id'], 'amount' => $_POST['amount']);
        $_SESSION['cart'][] = $ary;
        header('Location: product_list.php');
    }
}
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>商品詳細</title>
</head>
<body>
    <h1>商品詳細</h1>
    <p>商品名：<?php echo $result['name']; ?></p>
    <p>お値段：¥<?php echo $result['price']; ?></p>
    <p>紹介文</P>
    <?php echo $result['introduction']; ?><br />
    <?php
    if ($result['image']) {
        echo '<img src="../img/'.$result['image'].'">';
    } else {
        echo '<img src="../img/no_image.png">';
    }
    ?>
    <form action="" method="POST" style="margin:20px;">
        <input type="hidden" name="pro_id" value="<?php echo $result['id']; ?>">
        <span>数量：</span><input type="number" name="amount" value="1" min="1">
        <input type="submit" name="add_cart" value="カートに入れる">
    </form>
    <form action="product_review.php" method="POST" style="margin:20px;">
        <input type="hidden" name="id" value="<?php echo $result['id']; ?>">
        <input type="submit" name="" value="口コミを書く">
    </form>
    <?php echo 'いいね数　'.$favorite['COUNT(product_id)']; ?>
    <?php if(isset($favorited['product_id'])){ ?> <br>
        <form action="" method="POST" style="margin:20px; inlinebox">
        <input type="hidden" name="id" value="<?php echo $result['id']; ?>">
        <input type="submit" name="favorite_delete" value="いいねを取り消す">
    </form>
    <?php }elseif(!empty($_SESSION['user_id'])){ ?>
    <form action="" method="POST" style="margin:20px; inlinebox">
        <input type="hidden" name="id" value="<?php echo $result['id']; ?>">
        <input type="submit" name="favorite" value="いいね">
    </form>
    <?php } ?>


    <table border="1">
      <tr>
        <th style="width:100px;">ニックネーム</th>
        <th style="width:300px;">コメント</th>
        <th style="width:100px;">投稿者削除</th>
      </tr>
      <?php
      foreach($reviews as $review){
      ?>
      <tr>
        <td><?php echo $review['nickname']; ?></td>
        <td><?php echo $review['comment']; ?></td>
        <td><?php if($review['user_id']==$_SESSION['user_id']){ ?>
              <form action='' method="POST">
              <input type="hidden" name="pro_id" value="<?php echo $review['id'] ;?>">
              <input type="submit"  name="delete" value="削除"> 
              <?php } ?></form></td>
      </tr>
      <?php } ?>
    </table>

    <a href="../products/product_list.php">戻る</a><br />
</body>
</html>
