<?php
session_start();
session_regenerate_id(true);
require_once '../common/function.php';// for user-defined function

if (! isset($_SESSION['auth'])) {
    $_SESSION['not_login'] = 'ログインしてください！';
    header('Location: /ec_site/auth/login.php');
}

$db = dbConnect();
$sql = "SELECT * FROM products WHERE id = {$_POST['id']}";
// var_dump($sql);
$stmt = $db->prepare($sql);
$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);
//echo '<pre>';
//var_dump($result);
//echo '</pre>';

$errors = array();

if(isset($_POST['review'])){
    $post = sanitize($_POST);// HTML escape
    //exit;
    if ($post['nickname'] === '') {
        $errors['nickame'] = "ニックネームが入力されていません。";
    }elseif (strlen($post['nickname']) > 50) {
        $errors['nickame'] = "ニックネームが入力されていません。";
        "ニックネームは50文字以内にしてください。";
    }
    if($post['comment'] === '') {
        $errors['comment'] = "コメントが入力されていません。";
    }elseif (strlen($post['nickname']) > 140) {
        $errors['comment'] = "コメントは140文字以内にしてください。";
    }
    if(empty($errors)){
        // 口コミ登録
        try {
            $db = dbConnect();
            $db->beginTransaction();
            $nickname = $post['nickname'];
            $comment = $post['comment'];
            $product_id = intval($post['id']);
            $user_id = intval($_SESSION['user_id']);
            $sql = "INSERT INTO reviews(id, nickname, comment, product_id,user_id)VALUES(NULL, :nickname, :comment, :product_id, :user_id)";
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':nickname', $nickname, PDO::PARAM_STR);
            $stmt->bindParam(':comment', $comment, PDO::PARAM_STR);
            $stmt->bindParam(':product_id', $product_id, PDO::PARAM_INT);
            $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
            $stmt->execute();
            $db->commit();
            unset($nickname);
            unset($comment);
        } catch (PDOException $e) {
            $db->rollBack();
            echo "接続失敗:" .$e->getMessage(). "\n";
        } finally {
            $db = null;
            header('Location: product_detail.php');
        }
    }
}

?>

<!DOCTYPE html>
<html>
<body>
    <h1>口コミ登録</h1>
    <h1>商品詳細</h1>
    <p>商品名：<?php echo $result['name']; ?></p>
    <p>お値段：¥<?php echo $result['price']; ?></p>
    <?php
        echo "<ul>";
        foreach ($errors as $message) {
            echo "<div style='color:red'>";
            echo "<li>";
            echo  $message;
            echo "</li>";
            echo"</div>";
        }
        echo "</ul>";
    ?>
    <form action="" method="POST">
        ニックネーム<br>
        <input type="text" name="nickname" style="height:20px;" value="<?php
            if (isset($_POST['nickname'])){ echo $_POST['nickname']; }?>"><br><br>
        コメント<br>
        <textarea name="comment" style="height:120px;"><?php
            if (isset($_POST['comment'])) { echo $_POST['comment']; }?></textarea><br><br>
        <input type="hidden" name="id" value="<?php echo $result['id'];?>">
        <input type="submit" name="review" value="コメントを送信する" >
    </form>
</body>
</html>