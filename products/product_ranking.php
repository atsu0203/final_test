<?php
session_start();
session_regenerate_id(true);
require_once '../common/function.php';// for user-defined function

try {
    $db = dbConnect();
    $sql = "SELECT name, image, introduction, price, SUM(amount) AS total FROM  products  INNER JOIN order_details ON order_details.product_id = products.id GROUP BY products.id ORDER BY total DESC LIMIT 5";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchall(PDO::FETCH_ASSOC);

} catch (PDOException $e) {
    echo "接続失敗:" .$e->getMessage(). "\n";
} finally {
    $db = null;
}
?>

<!DOCTYPE html>
<html>
    <head>
      <meta charset="UTF-8">
      <title>人気ランキング</title>
    </head>
    <body>
    <h2>人気ランキング</h2>
    <table border="1">
      <tr>
        <th style="width:300px;">順位</th>
        <th style="width:300px;">商品名</th>
        <th style="width:300px;">商品画像</th>
        <th style="width:300px;">価格</th>
        <th style="width:300px;">紹介文</th>
        <th style="width:300px;">詳細</th>
      </tr>
      <?php
      $n = 0;
      foreach($result as $row){
        $n = $n + 1;
      ?>
      <tr>
        <td><?php echo($n); ?></td>
        <td><?php echo $row['name']; ?></td>
        <td><?php    if ($row['image']) {
              echo '<img width="200px" src="../img/'.$row['image'].'">';
                } else {
              echo '<img src="../img/no_image.png">';
                }?></td>
        <td><?php echo '¥'.$row['price']; ?></td>
        <td><?php echo $row['introduction']; ?></td>
        <td><form action="product_detail.php" method="POST"><button type="detail" name="id" value="<?php echo $row['id']; ?>">詳しく見る</button></form></td>
      </tr>
      <?php
      }
      ?>
    </table>
    <a href="product_list.php">商品一覧に戻る</a><br />
    <a href="../cart/cart.php">カートを見る</a><br />
    <a href="../auth/logout.php">ログアウト</a>
    </body>
</html>
